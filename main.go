package main

import (
	"encoding/json"
	"fmt"
	"os"
	"os/exec"
	"strings"
	"time"
)

func main() {
	if len(os.Args) == 4 {
		switch os.Args[1] {
		case "line":
			fmt.Printf(generate())
		case "json":
			tick := time.NewTicker(1 * time.Second)
			jtext := make(map[string]string)
			for {
				select {
				case <-tick.C:
					{
						jtext["text"] = generate()
						json.NewEncoder(os.Stdout).Encode(jtext)
					}
				}
			}
		default:
			usage()
		}
	} else {
		usage()
	}
}

func usage() {
	fmt.Printf("moomybattupower line mouse /org/freedesktop/UPower/devices/mouse_dev_EF_72_9C_5B_4C_AC <--- for print one line and stop\n")
	fmt.Printf("moomybattupower json mouse /org/freedesktop/UPower/devices/mouse_dev_EF_72_9C_5B_4C_AC <--- for print json and loop\n\n")
	os.Exit(0)
}

func generate() string {
	data := runBash("upower -d | grep -E \"percent|Device\"")
	line := strings.Split(data, "\n")
	state := 0
	for _, v := range line {
		linex := strings.Split(v, ":")
		if len(linex) != 2 {
			state = 0
			continue
		}
		head := strings.TrimSpace(linex[0])
		tail := strings.TrimSpace(linex[1])
		if head == "Device" && state == 0 {
			if tail == os.Args[3] {
				state = 1
				continue
			}
			state = 0
		} else if head == "percentage" && state == 1 {
			return strings.TrimSpace(os.Args[2] + ": " + tail)
		} else {
			state = 0
		}
	}
	return os.Args[2] + ": " + "--"
}

func runBash(com string) string {
	data, _ := exec.Command("bash", "-c", com).Output()
	return string(data)
}

func panicerr(err error) {
	if err != nil {
		panic(err)
	}
}
